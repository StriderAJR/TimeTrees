﻿namespace TimeTrees.ConsoleUI
{
    public interface ILogic
    {
        void Execute();
    }
}