using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class PrintPeopleLogic : ILogic
    {
        public void Execute()
        {
            PersonRepo personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            foreach (Person person in people)
            {
                Console.WriteLine(person);
            }
        }
    }
}