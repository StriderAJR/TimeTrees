using System;
using System.Collections.Generic;
using System.IO;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class LeapYearLogic : ILogic
    {
        public void Execute()
        {
            var people = new PersonRepo().GetAllPeople();

            List<Person> found = FindPeopleBornInLeapYear(people);
            Console.WriteLine($"Следующие люди родились в високосный год и их возраст менее 20 лет:");
            foreach (Person person in found)
            {
                Console.WriteLine($"{person.Name}, родился {person.BirthDate.ToShortDateString()}");
            }
        }

        private List<Person> FindPeopleBornInLeapYear(List<Person> peopleData)
        {
            List<Person> result = new List<Person>();
            foreach (Person person in peopleData)
            {
                int age = GetAge(person.BirthDate);

                if (DateTime.IsLeapYear(person.BirthDate.Year) && age < 20)
                {
                    result.Add(person);
                }
            }

            return result;
        }

        private int GetAge(DateTime birthDate)
        {
            int age = DateTime.Now.Year - birthDate.Year;
            if (DateTime.Now.DayOfYear < birthDate.DayOfYear)
                age = age - 1;
            return age;
        }
    }
}