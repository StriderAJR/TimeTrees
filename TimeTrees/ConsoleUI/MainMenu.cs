﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem {Text = "Список людей", Logic = new PrintPeopleLogic(), IsSelected = true},
            new MenuItem {Text = "Найти дельту дат между событиями", Logic = new DeltaLogic()},
            new MenuItem {Text = "Найти людей, родившихся в високосный год", Logic = new LeapYearLogic()},
            new MenuItem {Text = "Пример поиска людей", Logic = new FindLogic()},
            new MenuItem {Text = "Добавить данные", Logic = new AddPersonLogic()},
            new MenuItem {Text = "Выход"}
        };
    }
}
