using System;
using System.Collections.Generic;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class FindPersonMenu
    {
        private readonly string _findPromt;

        public FindPersonMenu(string findPromt = "ПОИСК ЛЮДЕЙ")
        {
            _findPromt = findPromt;
        }

        public Person FindPerson(List<Person> people)
        {
            List<Person> found = new List<Person>();
            string name = string.Empty;
            int? selectedIndex = null;
            Person selectedPerson = null;

            do
            {
                ConsoleHelper.ClearScreen();

                string hintText = "Начните вводить имя: ";
                Console.WriteLine(_findPromt);
                Console.CursorVisible = true;
                Console.WriteLine($"{hintText}{name}");

                found = string.IsNullOrEmpty(name) ? people : FilterPeople(people, name);

                PrintPeople(found, selectedIndex);

                Console.SetCursorPosition($"{hintText}{name}".Length, 1);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);

                if (char.IsLetter(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedIndex = null;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    name = name.Remove(name.Length - 1);
                }
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? 0
                        : selectedIndex + 1 < found.Count
                            ? selectedIndex + 1
                            : 0;
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex = !selectedIndex.HasValue
                        ? found.Count - 1
                        : selectedIndex - 1 < 0
                            ? found.Count - 1
                            : selectedIndex - 1;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    if (selectedIndex.HasValue)
                    {
                        selectedPerson = found[selectedIndex.Value];
                        break;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
            } while (true);

            ConsoleHelper.ClearScreen();

            return selectedPerson;
        }

        private void PrintPeople(List<Person> people, int? selectedIndex)
        {
            for (var i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                if (selectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                }

                Console.WriteLine(person);

                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        private List<Person> FilterPeople(List<Person> people, string name)
        {
            List<Person> result = new List<Person>();
            foreach (Person person in people)
            {
                if (person.Name.Contains(name, StringComparison.OrdinalIgnoreCase))
                {
                    result.Add(person);
                }
            }

            return result;
        }
    }
}