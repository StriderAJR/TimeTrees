using System;
using System.Collections.Generic;
using System.IO;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class DeltaLogic : ILogic
    {
        public void Execute()
        {
            var timeline = new TimelineEventRepo().GetAllTimelineEvents();

            (int years, int months, int days) = DeltaMinAndMaxDate(timeline);
            // void GetMinAndMaxDate(out DateTime min, out DateTime max);
            // Tuple<DateTime, DateTime> GetMinAndMaxDate(); new Tuple<DateTime, DateTime>>(dt1, dt2); var dates = GetMinAndMaxDate(); dates.Item1; dates.Item2;

            Console.WriteLine($"Между макс и мин датами прошло: {years} лет, {months} месяцев и {days} дней");
        }

        static (int, int, int) DeltaMinAndMaxDate(List<TimelineEvent> timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year,
                maxDate.Month - minDate.Month,
                maxDate.Day - minDate.Day);
        }

        static (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (var timeEvent in timeline)
            {
                if (timeEvent.StartDate < minDate) minDate = timeEvent.StartDate;
                if (timeEvent.StartDate > maxDate) maxDate = timeEvent.StartDate;
            }

            return (minDate, maxDate);
        }
    }
}