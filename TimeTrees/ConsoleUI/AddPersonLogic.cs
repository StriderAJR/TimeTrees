using System;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class AddPersonLogic : ILogic
    {
        public void Execute()
        {
            PersonRepo personRepo = new PersonRepo();
            var people = personRepo.GetAllPeople();

            Console.WriteLine("ДОБАВЛЕНИЕ НОВОГО ЧЕЛОВЕКА");

            Console.Write("Введите имя человека: ");
            string name = Console.ReadLine();

            Console.Write("Введите дату рождения человека: ");
            DateTime birthDate = Console.ReadLine().ToDateTime();

            Console.Write("Введите дату смерти человека, если она есть: ");
            DateTime? deathDate = null;
            string temp = Console.ReadLine();
            if (!string.IsNullOrEmpty(temp))
            {
                deathDate = temp.ToDateTime();
            }

            Person person = new Person(name, birthDate, deathDate);

            var array = new[] { "отец", "мать" };
            foreach (var relative in array)
            {
                Console.WriteLine($"Нажмите ENTER, если у человека неизвестен {relative}, и F, если нужно добавить.");
                ConsoleKeyInfo keyInfo;
                do
                {
                    keyInfo = Console.ReadKey();
                    if (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F)
                    {
                        Console.WriteLine("Неизвестная клавиша. Введите ENTER или F.");
                    }
                } while (keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.F);

                if (keyInfo.Key == ConsoleKey.F)
                {
                    FindPersonMenu fl = new FindPersonMenu("ПОИСК ЧЕЛОВЕКА");
                    Person found = fl.FindPerson(people);
                    person.AddRelative(found);
                }

                RePrint(person);
            }

            Console.Write("Вы уверены, что хотите сохранить данного человека? y/n");
            if (ConsoleHelper.GetYesNoAnswer())
            {
                personRepo.AddPerson(person);
            }
        }

        public void RePrint(Person person)
        {
            Console.WriteLine("ДОБАВЛЕНИЕ НОВОГО ЧЕЛОВЕКА");

            Console.Write("Введите имя человека: ");
            Console.WriteLine(person.Name);

            Console.Write("Введите дату рождения человека: ");
            Console.WriteLine(person.BirthDate.ToShortDateString());

            Console.Write("Введите дату смерти человека, если она есть: ");
            Console.WriteLine(person.DeathDate.HasValue ? person.DeathDate.Value.ToShortDateString() : string.Empty);

            var array = new[] { "отец", "мать" };
            for (var i = 0; i < person.Relatives.Count; i++)
            {
                Console.Write($"{array[i]}: ");

                var relative = person.Relatives[i];
                Console.WriteLine(relative.Name);
            }
        }
    }
}