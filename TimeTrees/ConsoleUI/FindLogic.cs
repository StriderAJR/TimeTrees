using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TimeTrees.Core;

namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public class FindLogic : ILogic
    {
        public void Execute()
        {
            var people = new PersonRepo().GetAllPeople();

            Person foundPerson = new FindPersonMenu().FindPerson(people);
            // Дальше можно найденного человека добавлять в родственники к другому человек
            // или делать что угодно другое, что требуется в программе

            ConsoleHelper.ClearScreen();
            Console.WriteLine($"Вы выбрали {foundPerson.Name}");
        }
    }
}