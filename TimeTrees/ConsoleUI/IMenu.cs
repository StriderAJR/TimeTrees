﻿namespace TimeTrees.ConsoleUI.ConsoleUI
{
    public interface IMenu
    {
        public void Draw();

        public void Prev();
        public void Next();
    }
}