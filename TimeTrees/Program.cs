﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Text;
using TimeTrees.Core;
using System.IO;

namespace Example
{
    class Program
    {
		public class SlideRecord
        {
			public SlideRecord(int id, SlideType slideType, string v)
			{
				Id = id;
				SlideType = slideType;
				Name = v;
			}

			public int SlideId { get; internal set; }
			public int Id { get; }
			public SlideType SlideType { get; }
			public string Name { get; }
		}

		public class VisitRecord
		{
			public VisitRecord(int userId, SlideRecord slide, DateTime dateTime)
			{
				UserId = userId;
				Slide = slide;
				DateTime = dateTime;
			}

			public int UserId { get; }
			public SlideRecord Slide { get; }
			public DateTime DateTime { get; }
		}

		public enum SlideType
		{
			theory, quiz, exercise
		}

		public static List<SlideRecord> ParseSlideRecords(IEnumerable<string> lines)
		{
			return lines.Skip(1)
				.Select(line => line.Split(';'))
				.Select(line =>
                {
					if (line.Length < 3
					|| !int.TryParse(line[0], out int id)
					|| !Enum.TryParse(line[1], true, out SlideType slideType))
						return null;
					return new SlideRecord(id, slideType, line[2]);
				})
				.Where(s => s != null)
				.ToList();
		}

		public static IEnumerable<VisitRecord> ParseVisitRecords(IEnumerable<string> lines, List<SlideRecord> slides)
		{
			return lines.Skip(1)
				.Select(line =>
                {
					var record = line.Split(';');
					try
					{
						var userId = int.Parse(record[0]);
						var slideId = int.Parse(record[1]);
						var slide = slides.First(x => x.Id == slideId);
						var date = record[2];
						var time = record[3];
						return new VisitRecord(
							userId,
							slide,
							DateTime.Parse(date) + TimeSpan.Parse(time)
						);
					}
					catch (Exception e)
					{
						throw new FormatException($"Wrong line [{line}]", e);
					}
				});
		}

		static void Main(string[] args)
        {
            //var slideStrings = File.ReadAllLines("slide.txt");
            //var slides = ParseSlideRecords(slideStrings);

            //var visitStrings = File.ReadAllLines("visits.txt");
            //var visits = ParseVisitRecords(visitStrings, slides);

            int index = 0;
			var list = new List<int> { 0, 0, 0, 0, 0 };
			var ints = list.Select(_ => ++index);

			Console.WriteLine(index);

			index = 10;
			foreach (var num in ints)
			{
				Console.Write($"{num} ");
			}
			Console.WriteLine();


			var actions = new List<Action>();
			for (int i = 0; i < 5; i++)
            {
				actions.Add(() => Console.Write($"{i} "));
			}

			// i = 5
			foreach (var action in actions)
			{
				action();
			}
		}
    }
}