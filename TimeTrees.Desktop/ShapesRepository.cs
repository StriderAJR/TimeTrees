﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TimeTrees.Desktop
{
    internal enum ConnectionType
    {
        /// <summary>
        /// Одноуровневая связь
        /// </summary>
        OneLevel,
        /// <summary>
        /// Связь между уровнями
        /// </summary>
        ParentChild
    }

    internal class ShapesRepository
    {
        private class RectangleConnectionInfo
        {
            public Rectangle Source { get; set; }
            public Rectangle Destination { get; set; }
            public Polyline Polyline { get; set; }

            public ConnectionType ConnectionType { get; set; }

            public bool IsActual { get; set; }
        }

        /// <summary>
        /// Связи прямоугольников между собой
        /// </summary>
        private List<RectangleConnectionInfo> rectangleInfos = new List<RectangleConnectionInfo>();

        public void AddRectangle(Rectangle rectangle)
        {
            if (rectangleInfos.Any(x => x.Source == rectangle || x.Destination == rectangle))
            {
                rectangleInfos.Add(new RectangleConnectionInfo
                {
                    Source = rectangle
                });
            }
        }

        public void AddConnection(Rectangle sourceRectangle, Rectangle destinationRectangle, Polyline polyline, ConnectionType connectionType, bool isActual)
        {
            var rectangleInfo = rectangleInfos.FirstOrDefault(x => (x.Source == sourceRectangle && x.Destination == destinationRectangle)
                                                                 || (x.Source == destinationRectangle && x.Destination == sourceRectangle));
            if (rectangleInfo == null)
            {
                var sourceRectangleInfo = rectangleInfos.FirstOrDefault(x => x.Source == sourceRectangle);
                var destRectangleInfo = rectangleInfos.FirstOrDefault(x => x.Source == destinationRectangle);

                // связываем фигуры, поэтому отдельная запись в списке для второй фигуры не нужна
                rectangleInfos.Remove(destRectangleInfo);

                rectangleInfos.Add(new RectangleConnectionInfo
                {
                    Source = sourceRectangle,
                    Destination = destinationRectangle,
                    ConnectionType = connectionType,
                    IsActual = isActual,
                    Polyline = polyline
                });
            }
        }

        public List<(Rectangle, Polyline)> GetConnections(Rectangle selectedRectangle)
        {
            List<(Rectangle, Polyline)> result = new List<(Rectangle, Polyline)>();

            result.AddRange(rectangleInfos
                .Where(x => x.Source == selectedRectangle)
                .Select(x => (x.Destination, x.Polyline))
                .ToList());

            result.AddRange(rectangleInfos
                .Where(x => x.Destination == selectedRectangle)
                .Select(x => (x.Source, x.Polyline))
                .ToList());

            return result;
        }
    }
}
