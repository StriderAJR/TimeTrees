﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using TimeTrees.Desktop.Tools;

namespace TimeTrees.Desktop
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        StatusBarUpdater statusBarUpdater;
        ShapesRepository shapesRepository;
        ToolArgs toolArgs;
        Tool currentTool = null;

        public MainWindow()
        {
            InitializeComponent();

            statusBarUpdater = new StatusBarUpdater(lblCoordinates, lblCurrentState, lblIsHover);
            shapesRepository = new ShapesRepository();
            toolArgs = new ToolArgs(this, canvas, statusBarUpdater, shapesRepository);

            currentTool = new ArrowTool(toolArgs);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new NewTreeNodeTool(toolArgs);
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new NewConnectionTool(toolArgs);
        }

        public void DisableTool()
        {
            if (currentTool != null) currentTool.Unload();
            currentTool = new ArrowTool(toolArgs);
        }
    }
}
