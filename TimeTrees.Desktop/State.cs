﻿namespace TimeTrees.Desktop
{
    public enum State
    {
        None,
        NewElement,
        NewConnection
    }
}
