﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace TimeTrees.Desktop
{
    internal static class CanvasExtenstions
    {
        public static Point Center(this Rectangle rect)
        {
            return new Point(Canvas.GetLeft(rect) + rect.Width / 2,
                             Canvas.GetTop(rect) + rect.Height / 2);
        }

        public static void UpdatePolylineBetweenRectangles(this Polyline polyline, Rectangle source, Rectangle destination)
        {
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            polyline.Points.Add(destination.Center());
        }

        public static void UpdatePolylineBetweenRectangleAndPoint(this Polyline polyline, Rectangle source, Point point)
        {
            polyline.Points.Clear();
            polyline.Points.Add(source.Center());
            polyline.Points.Add(point);
        }
    }
}
