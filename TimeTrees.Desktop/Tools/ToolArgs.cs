﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TimeTrees.Desktop.Tools
{
    internal class ToolArgs
    {
        public MainWindow MainWindow { get; private set; }
        public Canvas Canvas { get; private set; }
        public StatusBarUpdater StatusBarUpdater { get; private set; }
        public ShapesRepository ShapesRepository { get; private set; }

        public ToolArgs(MainWindow mainWindow, Canvas canvas, StatusBarUpdater statusBarUpdater, ShapesRepository shapesRepository)
        {
            MainWindow = mainWindow;
            Canvas = canvas;
            StatusBarUpdater = statusBarUpdater;
            ShapesRepository = shapesRepository;
        }
    }
}
