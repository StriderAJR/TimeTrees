﻿using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace TimeTrees.Desktop.Tools
{
    internal class ArrowTool : Tool
    {
        private Rectangle selectedRectangle;

        public ArrowTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseDown += OnMouseDown;
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseUp += OnMouseUp;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            selectedRectangle = hoveredShapes.FirstOrDefault(x => x is Rectangle) as Rectangle;
        }

        private void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            selectedRectangle = null;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(args.Canvas).X;
            var mouseY = e.GetPosition(args.Canvas).Y;

            var additionalInfo = GetHoveredShapesInfo();
            args.StatusBarUpdater.Update(State.None, e.GetPosition(args.Canvas), additionalInfo);

            if (e.LeftButton == MouseButtonState.Pressed && selectedRectangle != null)
            {
                Canvas.SetTop(selectedRectangle, mouseY);
                Canvas.SetLeft(selectedRectangle, mouseX);

                var connections = args.ShapesRepository.GetConnections(selectedRectangle);
                foreach((Rectangle destination, Polyline polyline) in connections)
                {
                    polyline.UpdatePolylineBetweenRectangles(selectedRectangle, destination);
                }
            }
        }

        public override void Unload()
        {
            args.Canvas.MouseDown -= OnMouseDown;
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseUp -= OnMouseUp;
            base.Dispose();
        }
    }
}