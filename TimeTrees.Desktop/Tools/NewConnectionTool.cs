﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TimeTrees.Desktop.Tools
{
    internal class NewConnectionTool : Tool
    {
        private List<Rectangle> selectedRects = new List<Rectangle>();
        private Polyline newConnection = null;

        public NewConnectionTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseDown += OnMouseDown;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(args.Canvas).X;
            var mouseY = e.GetPosition(args.Canvas).Y;
            var additionalInfo = GetHoveredShapesInfo();

            args.StatusBarUpdater.Update(State.NewConnection, e.GetPosition(args.Canvas), additionalInfo);

            if (selectedRects.Count == 1)
            {
                if (newConnection == null)
                {
                    newConnection = new Polyline();
                    newConnection.Stroke = Brushes.Black;

                    args.Canvas.Children.Add(newConnection);
                    Canvas.SetZIndex(newConnection, 1);
                }

                var selectedRect = selectedRects.First();
                newConnection.UpdatePolylineBetweenRectangleAndPoint(selectedRect, new Point(mouseX, mouseY));
            }
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectedRects.Count < 2)
            {
                var hoveredRect = hoveredShapes.FirstOrDefault(x => x is Rectangle) as Rectangle;
                if (hoveredRect != null) selectedRects.Add(hoveredRect);
            }

            if (selectedRects.Count == 2)
            {
                if (newConnection != null)
                {
                    newConnection.UpdatePolylineBetweenRectangles(selectedRects[0], selectedRects[1]);
                    args.ShapesRepository.AddConnection(selectedRects[0], selectedRects[1], newConnection, ConnectionType.OneLevel, true);
                    newConnection = null;
                    args.MainWindow.DisableTool();
                }
            }
        }

        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}