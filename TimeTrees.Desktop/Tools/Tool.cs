﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace TimeTrees.Desktop.Tools
{
    internal abstract class Tool : IDisposable
    {
        protected ToolArgs args;
        protected List<Shape> hoveredShapes = new List<Shape>();

        public Tool(ToolArgs args)
        {
            this.args = args;
            args.Canvas.MouseMove += BaseOnMouseMove;
        }

        protected void BaseOnMouseMove(object sender, MouseEventArgs e)
        {
            hoveredShapes = GetHoveredShapes();
            DrawHoverEffect(hoveredShapes);
        }

        protected string GetHoveredShapesInfo()
        {
            return string.Join(", ", hoveredShapes.Select(x => x.GetType().Name));
        }

        protected void DrawHoverEffect(List<Shape> hoveredShapes)
        {
            ClearHoverEffect();

            foreach (Shape shape in hoveredShapes)
            {
                shape.Effect = new DropShadowEffect
                {
                    Color = Colors.Red,
                    ShadowDepth = 0,
                    Direction = 0,
                    BlurRadius = 25
                };
            }
        }

        protected void ClearHoverEffect()
        {
            foreach (var element in args.Canvas.Children)
            {
                if (element is Shape shape)
                {
                    shape.Effect = null;
                }
            }
        }

        protected List<Shape> GetHoveredShapes()
        {
            List<Shape> hoveredShapes = new List<Shape>();
            foreach (var element in args.Canvas.Children)
            {
                if (element is Shape shape)
                {
                    if (shape.IsMouseOver)
                    {
                        hoveredShapes.Add(shape);
                    }
                }
            }

            return hoveredShapes;
        }

        public abstract void Unload();

        public void Dispose()
        {
            args.Canvas.MouseMove -= BaseOnMouseMove;
        }
    }
}
