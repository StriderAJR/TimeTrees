﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;

namespace TimeTrees.Desktop.Tools
{
    internal class NewTreeNodeTool : Tool
    {
        private Rectangle newRect = null;

        public NewTreeNodeTool(ToolArgs args) : base(args)
        {
            args.Canvas.MouseMove += OnMouseMove;
            args.Canvas.MouseDown += OnMouseDown;
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mouseX = e.GetPosition(args.Canvas).X;
            var mouseY = e.GetPosition(args.Canvas).Y;
            var additionalInfo = GetHoveredShapesInfo();

            args.StatusBarUpdater.Update(State.NewElement, e.GetPosition(args.Canvas), additionalInfo);

            if (newRect == null)
            {
                newRect = new Rectangle();
                newRect.Width = 50;
                newRect.Height = 50;
                newRect.Stroke = Brushes.Black;
                newRect.Fill = Brushes.White;

                args.Canvas.Children.Add(newRect);
                Canvas.SetZIndex(newRect, 5);
            }

            Canvas.SetTop(newRect, mouseY);
            Canvas.SetLeft(newRect, mouseX);
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            args.ShapesRepository.AddRectangle(newRect);
            newRect = null;
            args.MainWindow.DisableTool();
        }

        public override void Unload()
        {
            args.Canvas.MouseMove -= OnMouseMove;
            args.Canvas.MouseDown -= OnMouseDown;
            Dispose();
        }
    }
}
