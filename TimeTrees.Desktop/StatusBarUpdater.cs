﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace TimeTrees.Desktop
{
    internal class StatusBarUpdater
    {
        private Label lblCoordinates;
        private Label lblCurrentState;
        private Label lblIsHover;

        public StatusBarUpdater(Label lblCoordinates, Label lblCurrentState, Label lblIsHover)
        {
            this.lblCoordinates = lblCoordinates;
            this.lblCurrentState = lblCurrentState;
            this.lblIsHover = lblIsHover;
        }

        public void Update(State currentState, Point? point, string additionalInfo = null)
        {
            UpdateCurrentState(currentState);
            UpdateCoordinates(point);
            lblIsHover.Content = additionalInfo;
        }

        public void UpdateCurrentState(State currentState)
        {
            lblCurrentState.Content = currentState.ToString("G");
        }

        public void UpdateCoordinates(Point? point)
        {
            if (point != null)
            {
                lblCoordinates.Content = $"X:{point.Value.X} Y:{point.Value.Y}";
            }
        }
    }
}
