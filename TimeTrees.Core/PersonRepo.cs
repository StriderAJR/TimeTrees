﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.Core
{
    /// <summary>
    /// Эмуляция работы с БД для объекта Person
    /// </summary>
    public class PersonRepo : IPersonRepo
    {
        private static List<Person> people;

        public PersonRepo()
        {
            FileReader dr = new FileReader(Environment.CurrentDirectory, FileFormatType.Json);
            people ??= dr.GetPeople();
        }

        public List<Person> GetAllPeople()
        {
            return people.ToList();
        }

        public void AddPerson(Person person)
        {
            people.Add(person);
        }

        public void RemovePerson(Person person)
        {
            people.Remove(person);
        }

        public Person GetPersonById(int id)
        {
            return people.FirstOrDefault(x => x.Id == id);
        }

        public Person GetPersonByName(string name)
        {
            return people.FirstOrDefault(x => x.Name.Contains(name));
        }
    }
}
