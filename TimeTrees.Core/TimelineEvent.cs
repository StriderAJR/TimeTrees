using System;

namespace TimeTrees.Core
{
    public class TimelineEvent
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Совпадает с StartDate, если это не длящееся событие
        /// </summary>
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
    }
}