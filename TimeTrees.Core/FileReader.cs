using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace TimeTrees.Core
{
    public class FileReader
    {
        private const int personIdIndex = 0;
        private const int personNameIndex = 1;
        private const int personBirthDateIndex = 2;
        private const int personDeathDateIndex = 3;

        private const int timelineDateIndex = 0;
        private const int timelineDescriptionIndex = 1;

        private readonly string _initialPath;
        private const string _timelineFileName = "timeline";
        private const string _peopleFileName = "people";
        private readonly FileFormatType _fileType;
        private string _fileExtension => _fileType switch
        {
            FileFormatType.Cvs => "csv",
            FileFormatType.Json => "json",
            _ => throw new ArgumentOutOfRangeException()
        };

        public FileReader(string initialPath, FileFormatType fileType)
        {
            _initialPath = initialPath;
            _fileType = fileType;
        }

        public List<TimelineEvent> GetTimeEvents()
        {
            if (!File.Exists(Path.Combine(_initialPath, $"{_timelineFileName}.{_fileExtension}")))
                return new List<TimelineEvent>();

            if (_fileType == FileFormatType.Cvs)
            {
                return ReadTimeEventsFromCvs();
            }
            else if (_fileType == FileFormatType.Json)
            {
                return ReadTimeEventsFromJson();
            }

            return new List<TimelineEvent>();
        }

        public List<Person> GetPeople()
        {
            if (!File.Exists(Path.Combine(_initialPath, $"{_peopleFileName}.{_fileExtension}")))
                return new List<Person>();

            if (_fileType == FileFormatType.Cvs)
            {
                return ReadPeopleFromCvs();
            }
            else if (_fileType == FileFormatType.Json)
            {
                return ReadPeopleFromJson();
            }

            return new List<Person>();
        }

        private List<TimelineEvent> ReadTimeEventsFromCvs()
        {
            string[][] data = ReadData(Path.Combine(_initialPath, $"{_timelineFileName}.{_fileExtension}"));
            List<TimelineEvent> result = new List<TimelineEvent>();

            foreach (var elementData in data)
            {
                var date = elementData[timelineDateIndex].ToDateTime();
                TimelineEvent element = new TimelineEvent
                {
                    StartDate = date,
                    EndDate = date,
                    Description = elementData[timelineDescriptionIndex]
                };

                result.Add(element);
            }

            return result;
        }

        private List<TimelineEvent> ReadTimeEventsFromJson()
        {
            string json = File.ReadAllText(Path.Combine(_initialPath, $"{_peopleFileName}.{_fileExtension}"));
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(json);
        }

        private List<Person> ReadPeopleFromCvs()
        {
            string[][] data = ReadData(Path.Combine(_initialPath, $"{_peopleFileName}.{_fileExtension}"));
            List<Person> result = new List<Person>();

            foreach (var elementData in data)
            {
                Person element = new Person(
                    int.Parse(elementData[personIdIndex]),
                    elementData[personNameIndex],
                    elementData[personBirthDateIndex].ToDateTime(),
                    null
                );

                if (elementData.Length > 3)
                {
                    element.SetDeathDate(elementData[personDeathDateIndex].ToDateTime());
                }

                result.Add(element);
            }

            return result;
        }

        private List<Person> ReadPeopleFromJson()
        {
            string json = File.ReadAllText(Path.Combine(_initialPath, $"{_timelineFileName}.{_fileExtension}"));
            return JsonConvert.DeserializeObject<List<Person>>(json);
        }

        private string[][] ReadData(string path)
        {
            string[] data = File.ReadAllLines(path);
            string[][] splitData = new string[data.Length][];
            for (var i = 0; i < data.Length; i++)
            {
                var line = data[i];
                string[] parts = line.Split(";");
                splitData[i] = parts;
            }

            return splitData;
        }
    }
}