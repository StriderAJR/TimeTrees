using System.Collections.Generic;
using System.Linq;

namespace TimeTrees.Core
{
    public static class PeopleIdGenerator
    {
        public static int MaxId { get; set; }

        public static int GetNextId(List<Person> people)
        {
            return people.Max(x => x.Id) + 1;
        }
    }
}