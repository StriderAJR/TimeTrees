﻿using System.Collections.Generic;

namespace TimeTrees.Core
{
    public interface IPersonRepo
    {
        List<Person> GetAllPeople();
        void AddPerson(Person person);
        void RemovePerson(Person person);
        Person GetPersonById(int id);
        Person GetPersonByName(string name);
    }
}