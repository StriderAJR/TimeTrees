using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace TimeTrees.Core
{
    public class FileWriter
    {
        private const string TimelineFileName = "timeline";
        private const string PeopleFileName = "people";
        private readonly FileFormatType _fileType;
        private readonly string _peopleFilePath;
        private readonly string _timelineFilePath;

        public FileWriter(string initialPath, FileFormatType fileType)
        {
            _fileType = fileType;
            string fileExtension = fileType == FileFormatType.Cvs
                ? "csv"
                : "json";

            _peopleFilePath = Path.Combine(initialPath, $"{PeopleFileName}.{fileExtension}");
            _timelineFilePath = Path.Combine(initialPath, $"{TimelineFileName}.{fileExtension}");

            if (!File.Exists(_peopleFilePath)) File.WriteAllLines(_peopleFilePath, GeneratePeopleData());
            if (!File.Exists(_timelineFilePath)) File.WriteAllLines(_timelineFilePath, GenerateTimelineData());
        }

        public void SavePeople(List<Person> people)
        {
            string data = string.Empty;
            if (_fileType == FileFormatType.Cvs)
            {
                StringBuilder sb = new StringBuilder();
                foreach (Person person in people)
                {
                    sb.AppendLine($"{person.Id};" +
                                  $"{person.Name};" +
                                  $"{person.BirthDate.ToShortDateString()};" +
                                  $"{(person.DeathDate.HasValue ? person.DeathDate.Value.ToShortDateString() : string.Empty)}");
                }

                data = sb.ToString();
            }
            else if (_fileType == FileFormatType.Json)
            {
                data = JsonConvert.SerializeObject(people);
            }

            File.WriteAllText(_peopleFilePath, data);
        }

        public void SaveTimelineEvents(List<TimelineEvent> timeline)
        {
            string data = string.Empty;
            if (_fileType == FileFormatType.Cvs)
            {
                StringBuilder sb = new StringBuilder();
                foreach (TimelineEvent timelineEvent in timeline)
                {
                    sb.AppendLine($"{timelineEvent.StartDate};" +
                                  $"{timelineEvent.Description}");
                }
                data = sb.ToString();
            }
            else if (_fileType == FileFormatType.Json)
            {
                data = JsonConvert.SerializeObject(timeline);
            }

            File.WriteAllText(_timelineFilePath, data);
        }

        private string[] GenerateTimelineData()
        {
            return new[]
            {
                "1941;Битва за Москву;2",
                "1944;Белорусская операция;3",
                "1942;Битва за Кавказ;5"
            };
        }

        private string[] GeneratePeopleData()
        {
            return new[]
            {
                "1;Иосиф Родионович Апанасенко;1890;1943",
                "2;Павел Артемьевич Артемьев;1897;1979",
                "3;Иван Христофорович Баграмян;1897;1982",
                "4;Иван Александрович Богданов;1897;1942",
                "5;Семён Михайлович Будённый;1883;1973",
                "6;Климент Ефремович Ворошилов;1881;1969",
            };
        }
    }
}