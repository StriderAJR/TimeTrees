using System.Text.Json.Serialization;

namespace TimeTrees.Core
{
    public enum FileFormatType
    {
        Json, Cvs
    }
}