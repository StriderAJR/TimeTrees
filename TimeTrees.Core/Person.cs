using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeTrees.Core
{
    // TimeTrees.Console
    // в этом проекте ТОЛЬКО работа с консолью

    // TimeTrees.Core
    // все логические классы бизнес-логики, которые нам понадобится переиспользовать в WPF
    // Person, TimeLine, FileReader, FileWriter и т.д.
    
    // TimeTrees.Desktop - графических интерфейс WPF

    /// <summary>
    /// Класс для хранения информации о человеке
    /// </summary>
    public class Person
    {
        /// <summary> Уникальный идентификатор человека, который не повторяется </summary>
        public int Id { get; private set; }
        /// <summary> Имя человека </summary>
        public string Name { get; private set; }
        public DateTime BirthDate { get; private set; }
        public DateTime? DeathDate { get; private set; }

        private List<Person> _relatives = new List<Person>();
        public List<Person> Relatives
        {
            get { return _relatives.ToList(); }
        }

        public Person(string name, DateTime birthDate, DateTime? deathDate = null)
        {
            PeopleIdGenerator.MaxId++;
            Id = PeopleIdGenerator.MaxId;

            Name = name;
            BirthDate = birthDate;
            DeathDate = deathDate;
        }

        public Person(int id, string name, DateTime birthDate, DateTime? deathDate = null) : this(name, birthDate, deathDate)
        {
            if (id > PeopleIdGenerator.MaxId)
            {
                PeopleIdGenerator.MaxId = id;
                Id = id;
            }
        }

        private void Init(string name, DateTime birthDate, DateTime? deathDate)
        {
            Name = name;
            BirthDate = birthDate;
            DeathDate = deathDate;
        }

        /// <summary>
        /// Задать дату смерти человека
        /// </summary>
        /// <param name="deathDate">Дата смерти (не может быть позже, чем дата рождения)</param>
        /// <exception cref="ArgumentException">Исключение срабатывает, если дата смерти позже даты рождения</exception>
        public void SetDeathDate(DateTime deathDate)
        {
            if (BirthDate <= deathDate)
            {
                DeathDate = deathDate;
            }
            else
            {
                throw new ArgumentException("Дата смерти не может быть меньше даты рождения.");
            }
        }

        public void AddRelative(Person person)
        {
            _relatives.Add(person);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Id}\t" +
                          $"{Name}\t" +
                          $"{BirthDate.ToShortDateString()}\t" +
                          $"{(DeathDate.HasValue ? DeathDate.Value.ToShortDateString() : "жив")}");

            var array = new[] { "отец", "мать" };
            for (var i = 0; i < _relatives.Count; i++)
            {
                sb.AppendLine();
                Person relative = _relatives[i];
                sb.Append($"\t{array[i]}: {relative}");
            }

            return sb.ToString();
        }
    }
}