﻿using System.Collections.Generic;

namespace TimeTrees.Core
{
    public interface ITimelineEventRepo
    {
        List<TimelineEvent> GetAllTimelineEvents();
        void AddTimelineEvent(TimelineEvent TimelineEvent);
        void RemoveTimelineEvent(TimelineEvent TimelineEvent);
        TimelineEvent GetTimelineEventById(int id);
    }
}