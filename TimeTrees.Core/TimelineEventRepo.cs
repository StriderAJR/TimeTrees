﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrees.Core
{
    /// <summary>
    /// Эмуляция работы с БД для объекта TimelineEvent
    /// </summary>
    public class TimelineEventRepo : ITimelineEventRepo
    {
        private static List<TimelineEvent> timelineEvents;

        public TimelineEventRepo()
        {
            if (timelineEvents == null)
            {
                FileReader dr = new FileReader(Environment.CurrentDirectory, FileFormatType.Json);
                timelineEvents = dr.GetTimeEvents();
            }
        }

        public List<TimelineEvent> GetAllTimelineEvents()
        {
            return timelineEvents.ToList();
        }

        public void AddTimelineEvent(TimelineEvent TimelineEvent)
        {
            timelineEvents.Add(TimelineEvent);
        }

        public void RemoveTimelineEvent(TimelineEvent TimelineEvent)
        {
            timelineEvents.Remove(TimelineEvent);
        }

        public TimelineEvent GetTimelineEventById(int id)
        {
            return timelineEvents.FirstOrDefault(x => x.Id == id);
        }
    }
}
